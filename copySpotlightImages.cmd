@ECHO OFF
SETLOCAL

SET _source="C:\Users\Brad\AppData\Local\Packages\Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy\LocalState\Assets"

SET _dest="D:\Users\Brad\Pictures\Wallpapers\WindowsSpotlight\ztemp\raw"
SET _imgDest="D:\Users\Brad\Pictures\Wallpapers\WindowsSpotlight\ztemp\images"
SET _logFile="D:\Users\Brad\Desktop\SpotlightSync\log.txt"
REM ROBOCOPY %_source% %_dest% /NJH /NJS /NDL /NP /LOG:%_logFile% /TEE
ROBOCOPY %_source% %_dest% /NJH /NJS /NDL /NP /TEE

COPY %_dest% %_imgDest%\*.jpg
